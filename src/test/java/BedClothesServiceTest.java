import com.company.domains.BedClothes;
import com.company.dto.DataDTO;
import com.company.dto.ResponseEntity;
import com.company.services.BedClothesService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;


public class BedClothesServiceTest {

    private final BedClothesService bedClothesService = new BedClothesService();

    @Test
    public void findByAllTest() {
        String sort = "1";
        ResponseEntity<DataDTO<List<BedClothes>>> all = bedClothesService.findAll(sort);
        Assertions.assertTrue(all.getData().isSuccess(), "Find all method is not passed!");
    }

    @Test
    public void findByIDTest() {
        Long id = 1L;
        ResponseEntity<DataDTO<BedClothes>> responseEntity = bedClothesService.findByID(id);
        Assertions.assertTrue(responseEntity.getData().isSuccess(), "Find by id method is not passed!");
    }

    @Test
    public void findByPriceTest() {
        Double price = 19D;
        ResponseEntity<DataDTO<List<BedClothes>>> all = bedClothesService.findByPrice(price);
        List<BedClothes> list = all.getData().getBody().stream().filter(dishes -> dishes.getPrice().equals(price)).toList();
        Assertions.assertFalse(list.isEmpty(), "Find by price method is not passed!");
    }

    @Test
    public void findByQuantityTest() {
        Integer quantity = 1;
        ResponseEntity<DataDTO<List<BedClothes>>> all = bedClothesService.findByQuantity(quantity);
        Assertions.assertFalse(all.getData().isSuccess(), "Find by quantity method is not passed!");
    }

    @Test
    public void filterByPriceTest() {
        Double max = 10D;
        Double min = 5D;
        ResponseEntity<DataDTO<List<BedClothes>>> all = bedClothesService.filterByPrice(min, max);
        Assertions.assertTrue(all.getData().isSuccess(), "Filter by price method is not passed!!");
    }
}
