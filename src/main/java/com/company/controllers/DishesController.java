package com.company.controllers;

import com.company.domains.Dishes;
import com.company.dto.DataDTO;
import com.company.dto.ResponseEntity;
import com.company.services.DishesService;
import com.company.utils.BaseUtils;

import java.util.List;

public class DishesController implements BaseController {

    private final DishesService dishesService = new DishesService();

    public void findByName() {
        BaseUtils.print("Enter name: ");
        ResponseEntity<DataDTO<Dishes>> responseEntity = dishesService.findByName(BaseUtils.readText());
        BaseUtils.print(BaseUtils.gson.toJson(responseEntity));
    }

    @Override
    public void findAll(String sort) {
        ResponseEntity<DataDTO<List<Dishes>>> responseEntity = dishesService.findAll(sort);
        BaseUtils.print(BaseUtils.gson.toJson(responseEntity));
    }

    @Override
    public void findByID() {
        BaseUtils.print("Enter id: ");
        ResponseEntity<DataDTO<Dishes>> responseEntity = dishesService.findByID(BaseUtils.readLong());
        BaseUtils.print(BaseUtils.gson.toJson(responseEntity));
    }

    @Override
    public void findByPrice() {
        BaseUtils.print("Enter price: ");
        ResponseEntity<DataDTO<List<Dishes>>> responseEntity = dishesService.findByPrice(BaseUtils.readDouble());
        BaseUtils.print(BaseUtils.gson.toJson(responseEntity));
    }

    @Override
    public void findByQuantity() {
        BaseUtils.print("Enter quantity: ");
        ResponseEntity<DataDTO<List<Dishes>>> responseEntity = dishesService.findByQuantity(BaseUtils.readInteger());
        BaseUtils.print(BaseUtils.gson.toJson(responseEntity));
    }

    @Override
    public void filterByPrice() {
        BaseUtils.print("Enter min: ");
        Double min = BaseUtils.readDouble();
        BaseUtils.print("Enter max: ");
        Double max = BaseUtils.readDouble();
        ResponseEntity<DataDTO<List<Dishes>>> responseEntity = dishesService.filterByPrice(min, max);
        BaseUtils.print(BaseUtils.gson.toJson(responseEntity));
    }
}
