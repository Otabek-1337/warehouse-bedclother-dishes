package com.company.domains;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BedClothes {
    private Long id;
    private String color;
    private Double price;
    private Integer quantity;
}
