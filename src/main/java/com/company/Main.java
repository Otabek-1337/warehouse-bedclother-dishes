package com.company;

import com.company.ui.AppUI;
import com.company.utils.BaseUtils;

public class Main {

    static AppUI appUI = new AppUI();

    public static void main(String[] args) {
        BaseUtils.println("------ Author: Davronov Husan --------");
        BaseUtils.println("------ Email: khusan_davronov@student.itpu.uz ---------");
        BaseUtils.println("------ Creation date: since 20/04/23 17:12 ----------- ");
        appUI.run();
    }
}