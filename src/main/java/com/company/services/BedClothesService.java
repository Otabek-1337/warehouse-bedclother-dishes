package com.company.services;

import com.company.entity.BedClothesEntity;
import com.company.domains.BedClothes;
import com.company.dto.AppErrorDTO;
import com.company.dto.DataDTO;
import com.company.dto.ResponseEntity;
import com.company.exeptions.GenericNotFoundException;

import java.util.Comparator;
import java.util.List;

public class BedClothesService implements BaseService<BedClothes> {

    private final BedClothesEntity bedClothesDao = new BedClothesEntity();

    @Override
    public ResponseEntity<DataDTO<List<BedClothes>>> findAll(String sort) {
        try {
            List<BedClothes> bedClothesList = bedClothesDao.findAll();
            if (bedClothesList.isEmpty()) {
                throw new GenericNotFoundException("BedClothes not found!");
            }
            switch (sort) {
                case "2" -> bedClothesList.sort(Comparator.comparing(BedClothes::getColor));
                case "1" -> bedClothesList.sort(Comparator.comparing(BedClothes::getId));
                case "3" -> bedClothesList.sort(Comparator.comparing(BedClothes::getPrice));
                case "4" -> bedClothesList.sort(Comparator.comparing(BedClothes::getQuantity));
            }
            return new ResponseEntity<>(new DataDTO<>(bedClothesList));
        } catch (Exception e) {
            return new ResponseEntity<>(new DataDTO<>(AppErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    @Override
    public ResponseEntity<DataDTO<BedClothes>> findByID(Long id) {
        try {
            BedClothes bedClothesList = bedClothesDao.findAll().stream().filter(bedClothes ->
                    bedClothes.getId().equals(id)).findFirst().orElse(null);
            if (bedClothesList == null) {
                throw new GenericNotFoundException("BedClothes not found!");
            }
            return new ResponseEntity<>(new DataDTO<>(bedClothesList), 200);
        } catch (Exception e) {
            return new ResponseEntity<>(new DataDTO<>(AppErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    @Override
    public ResponseEntity<DataDTO<List<BedClothes>>> findByPrice(Double price) {
        try {
            List<BedClothes> bedClothesList = bedClothesDao.findAll().stream().filter(bedClothes
                    -> bedClothes.getPrice().equals(price)).toList();
            if (bedClothesList.isEmpty()) {
                throw new GenericNotFoundException("BedClothes not found!");
            }
            return new ResponseEntity<>(new DataDTO<>(bedClothesList), 200);
        } catch (Exception e) {
            return new ResponseEntity<>(new DataDTO<>(AppErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    @Override
    public ResponseEntity<DataDTO<List<BedClothes>>> findByQuantity(Integer quantity) {
        try {
            List<BedClothes> bedClothesList = bedClothesDao.findAll().stream().filter(bedClothes ->
                    bedClothes.getQuantity().equals(quantity)).toList();
            if (bedClothesList.isEmpty()) {
                throw new GenericNotFoundException("BedClothes not found!");
            }
            return new ResponseEntity<>(new DataDTO<>(bedClothesList), 200);
        } catch (Exception e) {
            return new ResponseEntity<>(new DataDTO<>(AppErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    @Override
    public ResponseEntity<DataDTO<List<BedClothes>>> filterByPrice(Double min, Double max) {
        try {
            List<BedClothes> bedClothesList = bedClothesDao.findAll().stream().filter(bedClothes ->
                    bedClothes.getPrice() >= min && bedClothes.getPrice() <= max).toList();
            if (bedClothesList.isEmpty()) {
                throw new GenericNotFoundException("BedClothes not found!");
            }
            return new ResponseEntity<>(new DataDTO<>(bedClothesList), 200);
        } catch (Exception e) {
            return new ResponseEntity<>(new DataDTO<>(AppErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    public ResponseEntity<DataDTO<List<BedClothes>>> findByColor(String color) {
        try {
            List<BedClothes> bedClothesList = bedClothesDao.findAll().stream().filter(bedClothes ->
                    bedClothes.getColor().equals(color)).toList();
            if (bedClothesList.isEmpty()) {
                throw new GenericNotFoundException("BedClothes not found!");
            }
            return new ResponseEntity<>(new DataDTO<>(bedClothesList), 200);
        } catch (Exception e) {
            return new ResponseEntity<>(new DataDTO<>(AppErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }
}
