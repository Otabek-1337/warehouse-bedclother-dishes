package com.company.services;

import com.company.dto.DataDTO;
import com.company.dto.ResponseEntity;

import java.util.List;


public interface BaseService<T> {
    ResponseEntity<DataDTO<List<T>>> findAll(String sort);

    ResponseEntity<DataDTO<T>> findByID(Long id);

    ResponseEntity<DataDTO<List<T>>> findByPrice(Double price);

    ResponseEntity<DataDTO<List<T>>> findByQuantity(Integer quantity);

    ResponseEntity<DataDTO<List<T>>> filterByPrice(Double min, Double max);
}
