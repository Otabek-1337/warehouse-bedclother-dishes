#  Warehouse Search System: Bed Linens and Dishes Warehouse



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/kumanboy17/warehouse-bedclother-dishes.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/kumanboy17/warehouse-bedclother-dishes/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***




## Task Requirements
### The application developed must meet the following requirements:

- After launching, display the following:
- The application name, which should correspond to your subject area, version, and creation date
- Information about the developer (e.g., name and email address)
- A list of the available commands (menu)
- Wait for a command to be input and the additional parameters for executing it.
- After entering the command and the required parameters, perform the calculations, print the result, and wait for the next command to be entered.
- One of the commands must exit the application.
- Read the inventory data from the pre-formatted text file.
- Allow users to search for products in the inventory based on various parameters.
- When a user searches for a product, display a list of matching products with their details (e.g., ID, name, category, price, quantity, etc.).
- If no matching products exist, display a message indicating no products were found.
- Allow users to see a list of all the products in the inventory, sorted by the selected order.
- Errors should be handled gracefully, displaying the appropriate error messages to the user.

## Functionality
### The application must be able to:

- Perform the tasks specified in the project requirements
- Take input from the user and provide output as expected
- Run efficiently, with minimal resource usage and no unnecessary delays or hang-ups

##  Code Quality
- The source code should be well-organized, readable, and maintainable. 
- It should follow best practices and adhere to the principles of object-oriented programming.
- 
- Use a Git repository for the source code. 
- This will allow the mentor to review the commit history and see the progress of the project over time. 
- The mentor will consider factors such as the frequency and quality of commits.

## Commands after running Main.java:

| command options    | command shortcut  |
|--------------------|:------------------|
| 1                  | Dishes            |
| 2                  | Bedclothes        |
| q                  | quit              |

## Commands for sorting :

| command options    | command shortcut  |
|--------------------|:------------------|
| 1                  | Find by name      |  
| 2                  |  Show all         |
| 3                  |  Find by id       |
| 4                  |  Find by price    |
| 5                  |  Find by quantity |
| 6                  |  Filter by price  |
| 0                  |  Back             |



## License
Open source Project
## Library
- Java version 19
- Maven Library 
